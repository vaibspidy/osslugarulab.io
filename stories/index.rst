.. title: Open Source Lugaru HD
.. slug: index
.. date: 2016-11-20 08:17:46 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

.. figure:: /images/lugaru-banner.png

What is Lugaru?
---------------

`Lugaru`_ (pronounced Loo-GAH-roo) is a cross-platform third-person action game.
The main character, Turner, is an anthropomorphic rebel bunny rabbit with impressive combat skills.
In his quest to find those responsible for slaughtering his village, he uncovers a far-reaching conspiracy
involving the corrupt leaders of the rabbit republic and the starving wolves from a nearby den.
Turner takes it upon himself to fight against their plot and save his fellow rabbits from slavery.

.. _Lugaru: http://www.wolfire.com/lugaru

Downloads
---------

Latest release: Version 1.1
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The OSS Lugaru project is proud to announce Lugaru HD 1.1, the first official
release of the open source Lugaru code!

See the `full release notes <https://gitlab.com/osslugaru/lugaru/tags/1.1>`_
for more details about the cool features it brings, and especially the
enhancements over the current Wolfire release. Download it now and play:

- `Linux 64-bit <https://bitbucket.org/osslugaru/lugaru/downloads/Lugaru-1.1-Linux-x86_64.tar.xz>`_
- `Mac OS X 64-bit <https://bitbucket.org/osslugaru/lugaru/downloads/Lugaru-1.1-MacOS-10.10-x86_64.zip>`_
- `Windows 32-bit <https://bitbucket.org/osslugaru/lugaru/downloads/Lugaru-1.1-Windows32.zip>`_
- `Windows 64-bit <https://bitbucket.org/osslugaru/lugaru/downloads/Lugaru-1.1-Windows64.zip>`_
- `Source tarball <https://bitbucket.org/osslugaru/lugaru/downloads/lugaru-1.1.tar.xz>`_

Development builds
~~~~~~~~~~~~~~~~~~

Latest builds of the `master branch <https://gitlab.com/osslugaru/lugaru/tree/master>`_:

- `Linux 64-bit (Clang) <https://gitlab.com/osslugaru/lugaru/builds/artifacts/master/download?job=build_clang>`_
- `Linux 64-bit (GCC) <https://gitlab.com/osslugaru/lugaru/builds/artifacts/master/download?job=build_gcc>`_
- `Windows 32-bit (MinGW32) <https://gitlab.com/osslugaru/lugaru/builds/artifacts/master/download?job=build_mingw32>`_
- `Windows 64-bit (MinGW64) <https://gitlab.com/osslugaru/lugaru/builds/artifacts/master/download?job=build_mingw64>`_

Builds for OSX (and potentially for Windows using MSVC) should be available in the future.
Please contact us if you want to help with those!

Source code
~~~~~~~~~~~

You can get the latest source code by cloning the `GitLab repository <https://gitlab.com/osslugaru/lugaru>`_
or by downloading an archive of the master branch (`zip <https://gitlab.com/osslugaru/lugaru/repository/archive.zip?ref=master>`_
or `tar.bz2 <https://gitlab.com/osslugaru/lugaru/repository/archive.tar.bz2?ref=master>`_).
